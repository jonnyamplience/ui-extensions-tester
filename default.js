"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var dc_extensions_sdk_1 = require("dc-extensions-sdk");
var onInit = function () { return __awaiter(void 0, void 0, void 0, function () {
    var SDK, value, output, btn, btnIm, btnVid, btnContent, x, staging, locales, contenttype, contentItem, params, paramDiv, formatted;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, dc_extensions_sdk_1.init()];
            case 1:
                SDK = _a.sent();
                return [4 /*yield*/, SDK.field.getValue()];
            case 2:
                value = _a.sent();
                output = document.getElementById('output');
                if (output) {
                    output.addEventListener('keyup', function () {
                        console.log('field value', output.value);
                        SDK.field.setValue(output.value);
                    });
                    if (value && typeof value !== 'object') {
                        output.innerHTML = JSON.stringify(value);
                    }
                }
                btn = document.getElementById('btn');
                if (btn) {
                    btn.addEventListener('click', function () {
                        SDK.frame.setHeight(Math.random() * 10);
                    });
                }
                btnIm = document.getElementById('btnIm');
                if (btnIm) {
                    btnIm.addEventListener('click', function () {
                        var getImage = function () { return __awaiter(void 0, void 0, void 0, function () { return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, SDK.mediaLink.getImage()];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        }); }); };
                        var image = getImage();
                        console.log('image', image);
                    });
                }
                btnVid = document.getElementById('btnVid');
                if (btnVid) {
                    btnVid.addEventListener('click', function () {
                        var getImage = function () { return __awaiter(void 0, void 0, void 0, function () { return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, SDK.mediaLink.getVideo()];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        }); }); };
                        var image = getImage();
                        console.log('video', image);
                    });
                }
                btnContent = document.getElementById('btnContent');
                if (btnContent) {
                    btnContent.addEventListener('click', function () {
                        var getImage = function () { return __awaiter(void 0, void 0, void 0, function () { return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, SDK.contentLink.get(['http://one.com', 'http://skillz.com'])];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        }); }); };
                        var image = getImage();
                        console.log('content', image);
                    });
                }
                x = SDK.field.schema;
                // const output = document.getElementById('output');
                // if (output) {
                //   output.innerHTML = jsonx;
                // }
                console.log('schema', x);
                console.log('value', value);
                staging = SDK.stagingEnvironment;
                console.log('stagin env', staging);
                locales = SDK.locales;
                console.log('LOCALES', locales);
                contenttype = SDK.contentType;
                console.log("contenttype", contenttype);
                return [4 /*yield*/, SDK.contentItem.getValue()];
            case 3:
                contentItem = _a.sent();
                console.log('content item', contentItem);
                params = SDK.params;
                console.log('params', params);
                paramDiv = document.getElementById('params');
                if (paramDiv) {
                    formatted = JSON.stringify(params);
                    paramDiv.innerHTML = formatted;
                }
                return [2 /*return*/];
        }
    });
}); };
// window.addEventListener('DOMContentLoaded', () => onInit());
onInit();
