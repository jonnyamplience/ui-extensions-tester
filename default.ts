import { init, SDK } from 'dc-extensions-sdk';
const onInit = async () => {
 const SDK:SDK = await init();
 const value = await SDK.field.getValue();
 const output = document.getElementById('output') as HTMLTextAreaElement;
 if (output) {
   //@ts-ignore
   if (SDK.field.schema.const) {
     output.disabled = true;
   }
   output.addEventListener('keyup', () => {
     console.log('field value', output.value);
     let value;
     if (SDK.field.schema.type === 'integer') {
       value = output.value === '' ? 0 : parseInt(output.value);
     } else {
       value = output.value;
     }
     SDK.field.setValue(value);
   });
   //@ts-ignore
   if (value || SDK.field.schema.const) {
     //@ts-ignore
     output.innerHTML = SDK.field.schema.const || value;
   }
 }
 const btn = document.getElementById('btn');
 if(btn) {
   btn.addEventListener('click', () => {
     SDK.frame.setHeight(Math.random() * 10);
   });
 }
 const btnIm = document.getElementById('btnIm');
 if(btnIm) {
   btnIm.addEventListener('click', () => {
     const getImage = async () => await SDK.mediaLink.getImage();
     const image = getImage();
     console.log('image', image);
   });
 }
 const btnVid = document.getElementById('btnVid');
 if(btnVid) {
   btnVid.addEventListener('click', () => {
     const getImage = async () => await SDK.mediaLink.getVideo();
     const image = getImage();
     console.log('video', image);
   });
 }
 const btnContent = document.getElementById('btnContent');
 if(btnContent) {
   btnContent.addEventListener('click', () => {
     const getImage = async () => await SDK.contentLink.get(['http://one.com', 'http://skillz.com']);
     const image = getImage();
     console.log('content', image);
   });
 }
 const x = SDK.field.schema;
 // const output = document.getElementById('output');
 // if (output) {
 //   output.innerHTML = jsonx;
 // }
 console.log('schema', x);
 console.log('value', value);
 const staging = SDK.stagingEnvironment;
 console.log('stagin env', staging);
 const locales = SDK.locales;
 console.log('LOCALES', locales);
 const contenttype = SDK.contentType;
 console.log ("contenttype", contenttype);
 const contentItem = await SDK.contentItem.getValue();
 console.log('content item', contentItem);
 const params = SDK.params;
 console.log('params', params);
}
// window.addEventListener('DOMContentLoaded', () => onInit());
onInit();