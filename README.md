### How do I get set up? ###

in the terminal:

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" (if you don't have npm)
brew install node
npm i parcel -g
npm i typescript -g
cd /Users/{your username}/Documents
git clone git@bitbucket.org:jonnyamplience/ui-extensions-tester.git
cd ./ui-extensions-tester
npm i
tsc -w (this will watch for changes to the files so you can make changes to the example extension).
(in a new tab)
parcel index.html
```
Copy the url outputted to the console (should be http://localhost:1234) use this as the URL when adding a ui:extension in a schema definition.